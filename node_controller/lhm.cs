using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using LibreHardwareMonitor.Hardware;

public class Startup
{
    static Computer c = new Computer()
    {
        IsGpuEnabled = true,
        IsCpuEnabled = true,
    };

    public async Task<object> Invoke(object input)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");

        float cpuTemp = -999;
        float gpuTemp = -999;
        float cpuLoad = -999;
        float gpuLoad = -999;

        c.Open();

        foreach (var hardware in c.Hardware)
        {
            if (hardware.HardwareType == HardwareType.Cpu)
            {
                hardware.Update();

                foreach (var sensor in hardware.Sensors)
                {
                    if (sensor.SensorType == SensorType.Temperature && sensor.Name.Contains("CPU Package"))
                    {
                        cpuTemp = sensor.Value.GetValueOrDefault();
                    }
                    if (sensor.SensorType == SensorType.Load && sensor.Name.Contains("CPU Total"))
                    {
                        cpuLoad = sensor.Value.GetValueOrDefault();
                    }
                }
            }
            if (hardware.HardwareType == HardwareType.GpuAmd || hardware.HardwareType == HardwareType.GpuNvidia || hardware.HardwareType == HardwareType.GpuIntel)
            {
                hardware.Update();

                foreach (var sensor in hardware.Sensors) {
                    if (sensor.SensorType == SensorType.Temperature && sensor.Name.Contains("GPU Core"))
                    {
                        gpuTemp = sensor.Value.GetValueOrDefault();
                    }
                    if (sensor.SensorType == SensorType.Load && sensor.Name.Contains("GPU Core"))
                    {
                        gpuLoad = sensor.Value.GetValueOrDefault();
                    }
                }
            }
        }
        return "{" +
            "\"cpuTemp\":" + cpuTemp.ToString("0.00") + "," +
            "\"gpuTemp\":" + gpuTemp.ToString("0.00") + "," +
            "\"cpuLoad\":" + cpuLoad.ToString("0.00") + "," +
            "\"gpuLoad\":" + gpuLoad.ToString("0.00") +
        "}";
    }
}
