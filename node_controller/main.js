const edge = require('edge-js');


const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

const getSensorDataFunc = edge.func({
    source: require('path').join(__dirname, 'lhm.cs'),
    references: ['LibreHardwareMonitorLib.dll']
});

const getSensorData = () =>
    new Promise((resolve, reject) =>
        getSensorDataFunc(0, (error, result) => error ? reject(error) : resolve(JSON.parse(result))));


(async () => {

    while (true) {
        console.log(".");
        console.log(await getSensorData());

        await delay(1000);
    }

})().then(() => { });
