#ifndef Effects_h
#define Effects_h

#include <Arduino.h>

class EffectStatic {
public:
  EffectStatic(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
  //
  void setColor(byte r, byte g, byte b);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  byte r, g, b;
};

class EffectAlarm {
public:
  EffectAlarm(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  int t;
  int p;
};

class EffectScan {
public:
  EffectScan(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
  //
  void setColor(byte r, byte g, byte b);
  void setInfinite(bool infinite);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  void setColors();
  //
  byte r, g, b;
  //
  int scanPause;
  int c0, c1, c2, c3, c4;
  int cR, cG, cB;
  int dir;
  int pause;
  bool useRandom;
  bool infinite;
};

class EffectQuake {
public:
  EffectQuake(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
  //
  void setMode(int mode);
  void setColor(bool r, bool g, bool b);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  bool r, g, b;
  //
  char pattern[52];
  byte pos;
};

class EffectGauge {
public:
  EffectGauge(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
  //
  void setColor(byte r, byte g, byte b);
  void setValue(byte value);
  void setOffset(byte value);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  byte r, g, b;
  byte value;
  byte offset;
};

class EffectRainbow {
public:
  EffectRainbow(int numPixels, void (*setPixel)(int, byte, byte, byte));
  void reset();
  void update(unsigned long ts);
  //
  void setMultiplier(int);
  void setSpeed(int);
private:
  int numPixels;
  void (*setPixel)(int, byte, byte, byte);
  unsigned long lastTs;
  //
  int offset;
  int multiplier;
  int speed;
};

#endif