#include <FastLED.h>

#include "status.h"
#include "effects.h"

#define RGB_PIN_0 2
#define RGB_PIN_1 3

/*
#define RGB_PIN_00 A0
#define RGB_PIN_01 A1
#define RGB_PIN_02 A2
#define RGB_PIN_03 A3
#define RGB_PIN_04 A4
#define RGB_PIN_05 A5

#define RGB_PIN_06 PD7
#define RGB_PIN_07 PD6
#define RGB_PIN_08 PD5
#define RGB_PIN_09 PD4
#define RGB_PIN_10 PD3
#define RGB_PIN_11 PD2
*/

#define NUMPIXELS_0 8
#define NUMPIXELS_1 24

#define NUMPIXELS 32
// #define NUMPIXELS 24 * 12

CRGB leds00[24];
CRGB leds01[24];
/*
CRGB leds02[24];
CRGB leds03[24];
CRGB leds04[24];
CRGB leds05[24];
CRGB leds06[24];
CRGB leds07[24];
CRGB leds08[24];
CRGB leds09[24];
CRGB leds10[24];
CRGB leds11[24];
*/

void saveSetColor(int c, byte r, byte g, byte b) {
  if (c >= 0 && c < NUMPIXELS_0) {
    leds00[c].setRGB(r, g, b);
    return;
  }
  c -= NUMPIXELS_0;
  if (c >= 0 && c < NUMPIXELS_1) {
    leds01[c].setRGB(r, g, b);
    return;
  }
}

EffectStatic effectOn(NUMPIXELS, &saveSetColor);
EffectStatic effectOff(NUMPIXELS, &saveSetColor);
EffectAlarm effectAlarm(NUMPIXELS, &saveSetColor);
EffectScan effectScanRed(NUMPIXELS, &saveSetColor);
EffectScan effectScanColored(NUMPIXELS, &saveSetColor);
EffectQuake effectQuake(NUMPIXELS, &saveSetColor);
EffectGauge effectGauge(NUMPIXELS, &saveSetColor);
EffectRainbow effectRainbow(NUMPIXELS, &saveSetColor);

Status status;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(9600);
  
  FastLED.addLeds<NEOPIXEL, RGB_PIN_0>(leds00, NUMPIXELS_0);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_1>(leds01, NUMPIXELS_1);

  /*
  FastLED.addLeds<NEOPIXEL, RGB_PIN_00>(leds00, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_01>(leds01, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_02>(leds02, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_03>(leds03, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_04>(leds04, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_05>(leds05, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_06>(leds06, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_07>(leds07, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_08>(leds08, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_09>(leds09, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_10>(leds10, 24);
  FastLED.addLeds<NEOPIXEL, RGB_PIN_11>(leds11, 24);
  */

  effectOn.setColor(255, 255, 255);
  effectOff.setColor(0, 0, 0);

  effectQuake.setMode(9);
  effectQuake.setColor(true, true, true);

  effectScanRed.setColor(255, 0, 0);
  effectScanRed.setInfinite(true);

  effectGauge.setColor(255, 0, 0);

  effectRainbow.setMultiplier(10);
  effectRainbow.setSpeed(5);
}

void loop() {
  unsigned long now = millis();

  io_serial(status);

  switch (status.mode) {
    case 0:
      effectOff.update(now);
      break;
    case 1:
      effectScanRed.update(now);
      break;
    case 2:
      effectScanColored.update(now);
      break;
    case 3:
      effectAlarm.update(now);
      break;
    case 4:
      effectQuake.update(now);
      break;
    case 5:
      effectRainbow.update(now);
      break;
    case 6:
      effectGauge.setValue(status.cpuTemp);
      effectGauge.setOffset(status.cpuLoad);
      effectGauge.update(now);
      break;
    case 7:
      effectOn.update(now);
      break;
  }

  // effectAlarm.update(now);


  FastLED.show(); 
  delay(10);
}
