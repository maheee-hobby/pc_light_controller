#include "status.h"

void io_serial(Status &status) {

  while (Serial.available() > 0) {
    int incomingByte = Serial.read();

    if (incomingByte >= 'A' && incomingByte <= 'Z') {
      Serial.println(F("Mode Change!"));
      Serial.println(incomingByte);
      status.mode = incomingByte - 'A';
    }

    if (incomingByte >= 'a' && incomingByte <= 'z') {
      Serial.println(F("Data Mode Change!"));
      Serial.println(incomingByte);
      status.serialScanMode = incomingByte;
    }

    if (incomingByte >= '0' && incomingByte <= '9') {
      Serial.println(F("Data Input!"));
      if (status.serialScanMode == 'a') {
        status.cpuTemp = incomingByte - '0';
        status.serialScanMode = 0;
      }
      if (status.serialScanMode == 'b') {
        status.cpuLoad = incomingByte - '0';
        status.serialScanMode = 0;
      }
    }
  }
}