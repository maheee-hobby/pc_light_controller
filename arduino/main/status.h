#ifndef Status_h
#define Status_h

#include <Arduino.h>

class Status {
public:
  Status();

  byte cpuTemp;
  byte gpuTemp;
  byte cpuLoad;
  byte gpuLoad;

  byte mode;
  byte serialScanMode;
};

#endif