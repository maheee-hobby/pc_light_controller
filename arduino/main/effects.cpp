#include <Arduino.h>
#include <avr/pgmspace.h>

#include "effects.h"

/*
 * Effect Static
 */
EffectStatic::EffectStatic(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  reset();
}

void EffectStatic::reset() {
  lastTs = 0;
}

void EffectStatic::update(unsigned long ts) {
  if (lastTs + 100 > ts) {
    return;
  }
  lastTs = ts;

  for (int i = 0; i < numPixels; ++i) {
    setPixel(i, r, g, b);
  }
}

void EffectStatic::setColor(byte r, byte g, byte b) {
  this->r = r;
  this->g = g;
  this->b = b;
}

/*
 * Effect Alarm
 */
EffectAlarm::EffectAlarm(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  reset();
}

void EffectAlarm::reset() {
  t = 0;
  p = 0;
  lastTs = 0;
}

void EffectAlarm::update(unsigned long ts) {
  p = 1 - p;
  if (p == 0 || lastTs + 100 > ts) {
    return;
  }
  lastTs = ts;

  int b = 0;
  for (int i = 0; i < numPixels; ++i) {
    if ((i + t) % 2 == 0) {
      setPixel(i, 0, 0, 0);
    } else {
      if (b % 2 == 0) {
        setPixel(i, 255, 0, 0);
      } else {
        setPixel(i, 0, 0, 255);
      }
      b = 1 - b;
    }
  }

  t = 1 - t;
}

/*
 * Effect Scan
 */
EffectScan::EffectScan(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  useRandom = true;
  infinite = false;
  reset();
}

void EffectScan::reset() {
  lastTs = 0;
  scanPause = 5;
  c0 = 0;
  c1 = 0;
  c2 = 0;
  c3 = 0;
  c4 = 0;
  cR = 0;
  cG = 0;
  cB = 0;
  dir = 1;
  pause = 0;
  setColors();
}

void EffectScan::setColors() {
  if (useRandom) {
    cR = random(0, 11);
    cG = random(0, 11);
    cB = random(0, 11);
  } else {
    cR = r / 25;
    cG = g / 25;
    cB = b / 25;
  }
}

void EffectScan::update(unsigned long ts) {
  if (lastTs + 100 > ts) {
    return;
  }
  lastTs = ts;

  if (infinite) {
    if (c0 == numPixels) {
      c0 = 0;
    }
  } else {
    if (dir == 1 && c0 == numPixels) {
      dir = -1;
      pause = scanPause;
    }
    if (dir == -1 && c0 == -1) {
      dir = 1;
      pause = scanPause;
    }
  }

  c4 = c3;
  c3 = c2;
  c2 = c1;
  c1 = c0;

  setPixel(c4, 0, 0, 0);
  setPixel(c3, 1 * cR, 1 * cG, 1 * cB);
  setPixel(c2, 5 * cR, 5 * cG, 5 * cB);
  setPixel(c1, 10 * cR, 10 * cG, 10 * cB);
  setPixel(c0, 25 * cR, 25 * cG, 25 * cB);

  if (pause > 0) {
    --pause;
    if (pause == 0) {
      setColors();
    }
  } else {
    c0 = c0 + dir;
  }
}

void EffectScan::setColor(byte r, byte g, byte b) {
  this->r = r;
  this->g = g;
  this->b = b;
  useRandom = false;
  setColors();
}

void EffectScan::setInfinite(bool infinite) {
  this->infinite = infinite;
}

/*
 * Effect Quake
 */

const char QUAKE_PATTERN_01[] PROGMEM = "mmnmmommommnonmmonqnmmo\0";                              // 1 FLICKER (first variety)
const char QUAKE_PATTERN_02[] PROGMEM = "abcdefghijklmnopqrstuvwxyzyxwvutsrqponmlkjihgfedcba\0";  // 2 SLOW STRONG PULSE
const char QUAKE_PATTERN_03[] PROGMEM = "mmmmmaaaaammmmmaaaaaabcdefgabcdefg\0";                   // 3 CANDLE (first variety)
const char QUAKE_PATTERN_04[] PROGMEM = "mamamamamama\0";                                         // 4 FAST STROBE
const char QUAKE_PATTERN_05[] PROGMEM = "jklmnopqrstuvwxyzyxwvutsrqponmlkj\0";                    // 5 GENTLE PULSE 1
const char QUAKE_PATTERN_06[] PROGMEM = "nmonqnmomnmomomno\0";                                    // 6 FLICKER (second variety)
const char QUAKE_PATTERN_07[] PROGMEM = "mmmaaaabcdefgmmmmaaaammmaamm\0";                         // 7 CANDLE (second variety)
const char QUAKE_PATTERN_08[] PROGMEM = "mmmaaammmaaammmabcdefaaaammmmabcdefmmmaaaa\0";           // 8 CANDLE (third variety)
const char QUAKE_PATTERN_09[] PROGMEM = "aaaaaaaazzzzzzzz\0";                                     // 9 SLOW STROBE (fourth variety)
const char QUAKE_PATTERN_10[] PROGMEM = "mmamammmmammamamaaamammma\0";                            // 10 FLUORESCENT FLICKER
const char QUAKE_PATTERN_11[] PROGMEM = "abcdefghijklmnopqrrqponmlkjihgfedcba\0";                 // 11 SLOW PULSE NOT FADE TO BLACK

const char *const QUAKE_PATTERNS[] PROGMEM = {
  QUAKE_PATTERN_01, QUAKE_PATTERN_02, QUAKE_PATTERN_03, QUAKE_PATTERN_04,
  QUAKE_PATTERN_05, QUAKE_PATTERN_06, QUAKE_PATTERN_07, QUAKE_PATTERN_08,
  QUAKE_PATTERN_09, QUAKE_PATTERN_10, QUAKE_PATTERN_11
};

EffectQuake::EffectQuake(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  this->r = true;
  this->g = true;
  this->b = true;

  reset();
}

void EffectQuake::reset() {
  lastTs = 0;
  pos = 0;
  setMode(0);
}

void EffectQuake::update(unsigned long ts) {
  if (lastTs + 100 > ts) {
    return;
  }
  lastTs = ts;

  if (pattern[pos] == 0) {
    pos = 0;
  }

  byte c = (pattern[pos] - 'a') * 9;
  for (int i = 0; i < numPixels; ++i) {
    setPixel(i, c * r, c * g, c * b);
  }

  ++pos;
}

void EffectQuake::setMode(int mode) {
  strcpy_P(pattern, (char *)pgm_read_word(&(QUAKE_PATTERNS[mode])));
}
void EffectQuake::setColor(bool r, bool g, bool b) {
  this->r = r;
  this->g = g;
  this->b = b;
}


/*
 * Effect Gauge
 */
EffectGauge::EffectGauge(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  this->r = 255;
  this->g = 255;
  this->b = 255;
  this->value = 0;
  this->offset = 0;

  reset();
}

void EffectGauge::reset() {
  lastTs = 0;
}

void EffectGauge::update(unsigned long ts) {
  if (lastTs + 100 > ts) {
    return;
  }
  lastTs = ts;

  for (int i = 0; i < min(numPixels, value); ++i) {
    setPixel((i + offset) % numPixels, r, g, b);
  }

  for (int i = min(numPixels, value); i < numPixels; ++i) {
    setPixel((i + offset) % numPixels, 0, 0, 0);
  }
}

void EffectGauge::setColor(byte r, byte g, byte b) {
  this->r = r;
  this->g = g;
  this->b = b;
}

void EffectGauge::setValue(byte value) {
  this->value = value;
}

void EffectGauge::setOffset(byte offset) {
  this->offset = offset;
}


/*
 * Effect Rainbow
 */
const byte RAINBOW_PATTERN[] = {
  0, 0, 0, 0, 1, 1, 2, 2, 3, 4, 4, 5, 6, 7, 8, 10, 11, 12, 14, 15, 17, 19, 21,
  22, 24, 26, 28, 31, 33, 35, 37, 40, 42, 45, 47, 50, 53, 55, 58, 61, 64, 67,
  70, 73, 76, 79, 82, 85, 88, 91, 95, 98, 101, 104, 108, 111, 114, 117, 121,
  124, 127, 131, 134, 138, 141, 144, 147, 151, 154, 157, 160, 164, 167, 170,
  173, 176, 179, 182, 185, 188, 191, 194, 197, 200, 202, 205, 208, 210, 213,
  215, 218, 220, 222, 224, 227, 229, 231, 233, 234, 236, 238, 240, 241, 243,
  244, 245, 247, 248, 249, 250, 251, 251, 252, 253, 253, 254, 254, 255, 255,
  255, 255, 255, 255, 255, 254, 254, 253, 253, 252, 251, 251, 250, 249, 248,
  247, 245, 244, 243, 241, 240, 238, 236, 234, 233, 231, 229, 227, 224, 222,
  220, 218, 215, 213, 210, 208, 205, 202, 200, 197, 194, 191, 188, 185, 182,
  179, 176, 173, 170, 167, 164, 160, 157, 154, 151, 147, 144, 141, 138, 134,
  131, 128, 124, 121, 117, 114, 111, 108, 104, 101, 98, 95, 91, 88, 85, 82, 79,
  76, 73, 70, 67, 64, 61, 58, 55, 53, 50, 47, 45, 42, 40, 37, 35, 33, 31, 28,
  26, 24, 22, 21, 19, 17, 15, 14, 12, 11, 10, 8, 7, 6, 5, 4, 4, 3, 2, 2, 1, 1,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

EffectRainbow::EffectRainbow(int numPixels, void (*setPixel)(int, byte, byte, byte)) {
  this->numPixels = numPixels;
  this->setPixel = setPixel;
  this->offset = 0;

  reset();
}

void EffectRainbow::reset() {
  lastTs = 0;
  offset = 0;
  multiplier = 1;
}

void EffectRainbow::update(unsigned long ts) {
  if (lastTs + 50 > ts) {
    return;
  }
  lastTs = ts;

  for (int i = 0; i < numPixels; ++i) {
    int j = i * multiplier + offset;
    setPixel(i,
             RAINBOW_PATTERN[(j) % 360],
             RAINBOW_PATTERN[(j + 120) % 360],
             RAINBOW_PATTERN[(j + 240) % 360]);
  }

  offset += speed;
  if (offset >= 360) {
    offset = 0;
  }
}

void EffectRainbow::setMultiplier(int multiplier) {
  this->multiplier = multiplier;
}

void EffectRainbow::setSpeed(int speed) {
  this->speed = speed;
}
