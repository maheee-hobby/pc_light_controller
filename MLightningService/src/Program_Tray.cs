﻿using System.Windows.Forms;
using MLightningService.ui;

namespace MLightningService
{
    internal static class Program_Tray
    {
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new MLApplicationContext());
        }
    }
}
