﻿using System;
using MLightningService.service;
using static MLightningService.util.Util;

namespace MLightningService
{
    internal static class ProgramTestMeasurements
    {
        static void Main()
        {
            // check if we are admin
            if (!IsCurrentUserInAdminGroup())
            {
                throw new Exception("Need admin rights!");
            }

            // create subservices
            HardwareMeasurements hardwareMeasurements = new HardwareMeasurements();

            // initialize subservices
            hardwareMeasurements.Start();
            hardwareMeasurements.Init();

            Console.WriteLine(hardwareMeasurements.GetReport());

            var result = hardwareMeasurements.GetMeasurements();
            Console.WriteLine(result.cpuLoad);
            Console.WriteLine(result.memoryLoad);


            // reset subservices
            hardwareMeasurements.Stop();

            Console.ReadLine();
        }
    }
}
