﻿using System;

namespace MLightningService.effect
{
    public class EffectLoadTempGauge
    {
        private readonly int ledCount;
        private readonly Action<int, int, int, int> setPixel;

        public EffectLoadTempGauge(int ledCount, Action<int, int, int, int> setPixel)
        {
            this.ledCount = ledCount;
            this.setPixel = setPixel;
        }

        public void Render(float load, float temp)
        {
            int l = Math.Min(ledCount, (int)Math.Round(load * ledCount) + 1);

            float rTemp = 1 - temp;
            int r = (int)Math.Round(temp * 150);
            int g = (int)Math.Round(rTemp * 150);

            setPixel(ledCount - 1, r, g, 0);

            for (int i = 0; i < ledCount; ++i)
            {
                if (i < l)
                {
                    if (i == l - 1)
                    {
                        setPixel(i, 255, 100, 100);
                    }
                    else
                    {
                        setPixel(i, r, g, 0);
                    }
                }
                else
                {
                    if (i < ledCount - 1)
                    {
                        setPixel(i, 0, 0, 100);
                    }
                }
            }
        }
    }
}
