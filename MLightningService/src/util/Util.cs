﻿using System;
using System.Linq;
using System.Security.Principal;

namespace MLightningService.util
{
    public class Util
    {
        public static long Now()
        {
            return DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }

        public static bool IsCurrentUserInAdminGroup()
        {
            var claims = new WindowsPrincipal(WindowsIdentity.GetCurrent()).Claims;
            var adminClaimID = new SecurityIdentifier(WellKnownSidType.BuiltinAdministratorsSid, null).Value;
            return claims.Any(c => c.Value == adminClaimID);
        }
    }
}
