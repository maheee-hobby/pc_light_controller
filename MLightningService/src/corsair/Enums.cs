﻿namespace MLightningService.corsair
{
    public enum ControlType
    {
        ExclusiveLightingControl = 0
    };

    public enum DeviceType
    {
        Unknown = 0,
        Mouse = 1,
        Keyboard = 2,
        Headset = 3,
        Mousemat = 4,
        HeadsetStand = 5,
        CommanderPro = 6,
        LightingNodePro = 7,
        MemoryModule = 8,
        Cooler = 9,
        Motherboard = 10,
        GraphicsCard = 11,
        Touchbar = 12
    };

    public enum Error
    {
        Success,
        ServerNotFound,
        NoControl,
        ProtocolHandshakeMissing,
        IncompatibleProtocol,
        InvalidArguments
    };
}
