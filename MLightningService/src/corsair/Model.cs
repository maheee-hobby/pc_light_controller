﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MLightningService.corsair
{

    public class ProtocolDetails
    {
        private readonly ProtocolDetailsStr data;

        public string SdkVersion { get { return Marshal.PtrToStringAnsi(data.sdkVersion); } }
        public string ServerVersion { get { return Marshal.PtrToStringAnsi(data.serverVersion); } }
        public int SdkProtocolVersion { get { return data.sdkProtocolVersion; } }
        public int ServerProtocolVersion { get { return data.serverProtocolVersion; } }
        public byte BreakingChanges { get { return data.breakingChanges; } }

        internal ProtocolDetails(ProtocolDetailsStr protocolDetailsStr)
        {
            data = protocolDetailsStr;
        }
    }

    public class DeviceInfo
    {
        private readonly DeviceInfoStr data;

        public DeviceType Type { get { return data.type; } }
        public string Model { get { return Marshal.PtrToStringAnsi(data.model); } }
        public int PhysicalLayout { get { return data.physicalLayout; } }
        public int LogicalLayout { get { return data.logicalLayout; } }
        public int CapsMask { get { return data.capsMask; } }
        public int LedsCount { get { return data.ledsCount; } }

        internal DeviceInfo(DeviceInfoStr deviceInfoStr)
        {
            data = deviceInfoStr;
        }
    }

    public class LedPositions
    {
        private readonly LedPositionsStr data;

        public int NumberOfLeds { get { return data.numberOfLed; } }

        public List<LedPosition> Positions
        {
            get
            {
                List<LedPosition> result = new List<LedPosition>();

                var ptr = data.pLedPosition;
                for (int i = 0; i < data.numberOfLed; ++i)
                {
                    var l = Marshal.PtrToStructure<LedPositionStr>(ptr);
                    result.Add(new LedPosition(l));
                    ptr = new IntPtr(ptr.ToInt64() + Marshal.SizeOf(typeof(LedPositionStr)));
                }

                return result;
            }
        }

        internal LedPositions(LedPositionsStr ledPositionsStr)
        {
            data = ledPositionsStr;
        }
    }

    public class LedPosition
    {
        private readonly LedPositionStr data;

        public int LedId { get { return data.led_id; } }
        public double Top { get { return data.top; } }
        public double Left { get { return data.left; } }
        public double Height { get { return data.height; } }
        public double Width { get { return data.width; } }

        internal LedPosition(LedPositionStr ledPositionStr)
        {
            data = ledPositionStr;
        }
    }

    public class LedColor
    {
        private readonly int ledId;
        private readonly int r;
        private readonly int g;
        private readonly int b;

        internal LedColorStr Data
        {
            get
            {
                return new LedColorStr
                {
                    led_id = ledId,
                    r = r,
                    g = g,
                    b = b
                };
            }
        }

        public LedColor(int ledId, int r, int g, int b)
        {
            this.ledId = ledId;
            this.r = r;
            this.g = g;
            this.b = b;
        }
    }
}
