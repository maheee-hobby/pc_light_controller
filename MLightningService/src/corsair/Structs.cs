﻿using System;
using System.Runtime.InteropServices;

namespace MLightningService.corsair
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct ProtocolDetailsStr
    {
        internal IntPtr sdkVersion;
        internal IntPtr serverVersion;
        internal int sdkProtocolVersion;
        internal int serverProtocolVersion;
        internal byte breakingChanges;
    };

    [StructLayout(LayoutKind.Sequential)]
    internal class DeviceInfoStr
    {
        internal DeviceType type;
        internal IntPtr model;
        internal int physicalLayout;
        internal int logicalLayout;
        internal int capsMask;
        internal int ledsCount;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal class LedPositionsStr
    {
        internal int numberOfLed;
        internal IntPtr pLedPosition;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal class LedPositionStr
    {
        internal int led_id;
        internal double top;
        internal double left;
        internal double height;
        internal double width;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal class LedColorStr
    {
        internal int led_id;
        internal int r;
        internal int g;
        internal int b;
    };
}
