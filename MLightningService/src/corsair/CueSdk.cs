﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MLightningService.corsair
{
    public static class CueSdk
    {
        private const string DLL_PATH = "res/CUESDK.x64_2019.dll";

        private static IntPtr dllHandle = IntPtr.Zero;

        [DllImport("kernel32.dll")]
        private static extern IntPtr LoadLibrary(string dllToLoad);

        [DllImport("kernel32.dll")]
        private static extern bool FreeLibrary(IntPtr dllHandle);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetProcAddress(IntPtr dllHandle, string name);

        private static T GetDelegate<T>(string name)
        {
            return Marshal.GetDelegateForFunctionPointer<T>(GetProcAddress(dllHandle, name));
        }

        public static void LoadSdk()
        {
            if (dllHandle != IntPtr.Zero)
            {
                return;
            }

            dllHandle = LoadLibrary(DLL_PATH);

            requestControlPtr = GetDelegate<RequestControlPtr>("CorsairRequestControl");
            releaseControlPtr = GetDelegate<ReleaseControlPtr>("CorsairReleaseControl");
            performProtocolHandshakePtr = GetDelegate<PerformProtocolHandshakePtr>("CorsairPerformProtocolHandshake");

            getDeviceCountPtr = GetDelegate<GetDeviceCountPtr>("CorsairGetDeviceCount");
            getDeviceInfoPtr = GetDelegate<GetDeviceInfoPtr>("CorsairGetDeviceInfo");
            getLedPositionsByDeviceIndexPtr = GetDelegate<GetLedPositionsByDeviceIndexPtr>("CorsairGetLedPositionsByDeviceIndex");

            setLedsColorsBufferByDeviceIndexPtr = GetDelegate<SetLedsColorsBufferByDeviceIndexPtr>("CorsairSetLedsColorsBufferByDeviceIndex");
            setLedsColorsFlushBufferPtr = GetDelegate<SetLedsColorsFlushBufferPtr>("CorsairSetLedsColorsFlushBuffer");

            getLastErrorPtr = GetDelegate<GetLastErrorPtr>("CorsairGetLastError");
        }

        public static void UnloadSdk()
        {
            if (dllHandle == IntPtr.Zero) return;

            while (FreeLibrary(dllHandle)) ;
            dllHandle = IntPtr.Zero;
        }

        /*
         * 
         */
        private static RequestControlPtr requestControlPtr;
        private static ReleaseControlPtr releaseControlPtr;
        private static PerformProtocolHandshakePtr performProtocolHandshakePtr;

        private static GetDeviceCountPtr getDeviceCountPtr;
        private static GetDeviceInfoPtr getDeviceInfoPtr;
        private static GetLedPositionsByDeviceIndexPtr getLedPositionsByDeviceIndexPtr;

        private static SetLedsColorsBufferByDeviceIndexPtr setLedsColorsBufferByDeviceIndexPtr;
        private static SetLedsColorsFlushBufferPtr setLedsColorsFlushBufferPtr;

        private static GetLastErrorPtr getLastErrorPtr;

        /*
         * 
         */
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate bool RequestControlPtr(ControlType accessMode);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate bool ReleaseControlPtr(ControlType accessMode);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate ProtocolDetailsStr PerformProtocolHandshakePtr();


        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int GetDeviceCountPtr();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate DeviceInfoStr GetDeviceInfoPtr(int deviceIndex);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate LedPositionsStr GetLedPositionsByDeviceIndexPtr(int deviceIndex);


        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate bool SetLedsColorsBufferByDeviceIndexPtr(int device, int size, IntPtr ledsColors);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate bool SetLedsColorsFlushBufferPtr();


        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate Error GetLastErrorPtr();

        /*
         * 
         */
        public static bool RequestControl(ControlType accessMode)
        {
            return requestControlPtr(accessMode);
        }

        public static bool ReleaseControl(ControlType accessMode)
        {
            return releaseControlPtr(accessMode);
        }

        public static ProtocolDetails PerformProtocolHandshake()
        {
            return new ProtocolDetails(performProtocolHandshakePtr());
        }


        public static int GetDeviceCount()
        {
            return getDeviceCountPtr();
        }

        public static DeviceInfo GetDeviceInfo(int deviceIndex)
        {
            return new DeviceInfo(getDeviceInfoPtr(deviceIndex));
        }

        public static LedPositions GetLedPositionsByDeviceIndex(int deviceIndex)
        {
            return new LedPositions(getLedPositionsByDeviceIndexPtr(deviceIndex));
        }


        public static bool SetLedsColorsBufferByDeviceIndex(int device, List<LedColor> colors)
        {
            int structSize = Marshal.SizeOf(typeof(LedColorStr));

            IntPtr ptr = Marshal.AllocHGlobal(structSize * colors.Count);

            IntPtr addPtr = new IntPtr(ptr.ToInt64());

            foreach (var color in colors)
            {
                Marshal.StructureToPtr(color.Data, addPtr, false);
                addPtr = new IntPtr(addPtr.ToInt64() + structSize);
            }

            var result = setLedsColorsBufferByDeviceIndexPtr(device, colors.Count, ptr);
            Marshal.FreeHGlobal(ptr);
            return result;
        }

        public static bool SetLedsColorsFlushBuffer()
        {
            return setLedsColorsFlushBufferPtr();
        }


        public static Error GetLastError()
        {
            return getLastErrorPtr();
        }
    }
}
