﻿using MLightningService.effect;
using MLightningService.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLightningService.effectkit
{
    public class EffectKitDemo : IEffectKit
    {
        private List<Device> devices;
        private Action<int, int, int, int> group;
        private int i = 0;
        private int im1 = 0;
        private int im2 = 0;
        private int im3 = 0;

        private EffectRainbow effectRainbow;
        /*
                ("ENE_RGB_For_ASUS0", 8), // RAM 1
                ("ENE_RGB_For_ASUS1", 8), // RAM 2
                ("Mainboard_Master",  3), // Mainboard
                ("AddressableStrip 1", 24), // LED Strip
                ("AddressableStrip 2", 16), // Graphics Card Holder
                ("AddressableStrip 3",  8), // Front Fan
                ("H150i ELITE",        9999), // Cooler (knows amount automatically)
                ("Lighting Node CORE", 9999), // Fans (Top & Bottom) (knows amount automatically)
         */

        public void Init(List<Device> devices)
        {
            this.devices = devices;

            group = (int i, int r, int g, int b) =>
            {
                i = i % 8;
                devices[0].SetPixel(i, r, g, b);
                devices[1].SetPixel(i, r, g, b);

                devices[3].SetPixel(i, r, g, b);
                devices[3].SetPixel(i + 8, r, g, b);
                devices[3].SetPixel(i + 16, r, g, b);

                devices[4].SetPixel(i, r, g, b);
                devices[4].SetPixel(i + 8, r, g, b);

                devices[5].SetPixel(i, r, g, b);

                devices[6].SetPixel(i, r, g, b);
                devices[6].SetPixel(i + 8, r, g, b);

                var ip2 = ((i + 2) % 8);
                devices[7].SetPixel(ip2, r, g, b);
                devices[7].SetPixel(i + 8, r, g, b);
                devices[7].SetPixel(i + 16, r, g, b);
                devices[7].SetPixel(ip2 + 24, r, g, b);
                devices[7].SetPixel(ip2 + 32, r, g, b);
                devices[7].SetPixel(i + 40, r, g, b);

                if (i < 3)
                {
                    devices[2].SetPixel(i, r, g, b);
                }
            };

            effectRainbow = new EffectRainbow(1, 2, 20, (int _, int r, int g, int b) =>
            {
                group(im3, 0, 0, 0);
                group(im2, r / 5, g / 5, b / 5);
                group(im1, r / 2, g / 2, b / 2);
                group(i, r, g, b);
            });
        }

        public bool Step(HardwareMeasurements.Measurements measurements)
        {
            effectRainbow.Render();

            im3 = im2;
            im2 = im1;
            im1 = i;

            if (++i >= 8)
            {
                i = 0;
            }

            return false;
        }
    }
}
