﻿using MLightningService.effect;
using MLightningService.service;
using System;
using System.Collections.Generic;
using static MLightningService.service.HardwareMeasurements;

namespace MLightningService.effectkit
{
    public class EffectKitDefault2 : IEffectKit
    {
        private List<Device> devices;

        private EffectLoadTempGauge cpuLoadTempGauge;
        private EffectLoadTempGauge memoryLoadTempGauge;
        private EffectLoadTempGauge gpuLoadTempGauge;
        private EffectWave ledStripWave;

        public void Init(List<Device> devices)
        {
            this.devices = devices;

            cpuLoadTempGauge = new EffectLoadTempGauge(8, (int i, int r, int g, int b) =>
            {
                devices[0].SetPixel(7 - i, r, g, b);
            });

            memoryLoadTempGauge = new EffectLoadTempGauge(8, (int i, int r, int g, int b) =>
            {
                devices[1].SetPixel(7 - i, r, g, b);
            });

            gpuLoadTempGauge = new EffectLoadTempGauge(16, (int i, int r, int g, int b) =>
            {
                devices[4].SetPixel(i, r, g, b);
            });

            ledStripWave = new EffectWave(24, 2, 20, (int i, int r, int g, int b) =>
            {
                var lightningNode = devices[7];
                var cpuCooler = devices[6];
                var rgbMainboard = devices[2];
                var rgbStrip1 = devices[3];
                var rgbStrip3 = devices[5];

                r /= 4;
                g /= 4;
                b /= 4;

                // first one
                if (i == 0)
                {
                    for (int j = lightningNode.LedCount / 2; j < lightningNode.LedCount; ++j)
                    {
                        lightningNode.SetPixel(j, r, g, b);
                    }
                }

                // middle
                if (i == 12)
                {
                    cpuCooler.SetPixels(r, g, b);
                    rgbMainboard.SetPixels(r, g, b);
                    rgbStrip3.SetPixels(r, g, b);
                }

                // last one
                if (i == 24 - 1)
                {
                    for (int j = 0; j < lightningNode.LedCount / 2; ++j)
                    {
                        lightningNode.SetPixel(j, r, g, b);
                    }
                }

                rgbStrip1.SetPixel(i, r, g, b);
            });
        }

        public bool Step(Measurements measurements)
        {
            if (devices == null)
            {
                return false;
            }

            // CPU Load/Temp
            if (measurements.cpuLoad != null)
            {
                float load = (float)measurements.cpuLoad / 100.0f;
                float temp = 0;
                if (measurements.cpuTemp != null)
                {
                    temp = Math.Max(0, ((float)measurements.cpuTemp - 40) / 60);
                }

                cpuLoadTempGauge.Render(load, temp);
            }
            else
            {
                // devices[1].SetPixels(0, 0, 100);
                devices[0].SetPixels(0, 0, 100);
            }

            // Memory Load
            if (measurements.memoryLoad != null)
            {
                float load = (float)measurements.memoryLoad / 100.0f;
                float temp = 0;
                memoryLoadTempGauge.Render(load, temp);
            }
            else
            {
                devices[1].SetPixels(0, 0, 100);
            }

            // GPU Load/Temp
            if (measurements.gpuLoad != null)
            {
                float load = (float)measurements.gpuLoad / 100.0f;
                float temp = 0;
                if (measurements.gpuTemp != null)
                {
                    temp = Math.Max(0, ((float)measurements.gpuTemp - 40) / 30);
                }

                gpuLoadTempGauge.Render(load, temp);
            }
            else
            {
                devices[4].SetPixels(0, 0, 100);

            }

            // Rainbow
            ledStripWave.Render();

            return false;
        }
    }
}
