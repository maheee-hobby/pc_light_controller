﻿using MLightningService.service;
using System;

namespace MLightningService.effectkit
{
    public class EffectKitScan : EffectKitSimple
    {
        private int i = 0;

        private void SetScanPixel(int i, int r, int g, int b)
        {
            SetPixel(i, r * 25, g * 25, b * 25);
            --i;
            if (i < 0)
                i += ledCount;
            SetPixel(i, r * 10, g * 10, b * 10);
            --i;
            if (i < 0)
                i += ledCount;
            SetPixel(i, r * 5, g * 5, b * 5);
            --i;
            if (i < 0)
                i += ledCount;
            SetPixel(i, r * 1, g * 1, b * 1);
            --i;
            if (i < 0)
                i += ledCount;
            SetPixel(i, 0, 0, 0);
        }

        public override bool Step(HardwareMeasurements.Measurements measurements)
        {
            if (firstRun)
            {
                SetPixels(0, 0, 0);
                firstRun = false;
            }

            SetScanPixel(++i, 10, 10, 10);
            if (i >= ledCount)
            {
                i = 0;
                return true;
            }

            return false;
        }
    }
}
