﻿using MLightningService.service;
using System.Collections.Generic;

namespace MLightningService.effectkit
{
    public abstract class EffectKitSimple : IEffectKit
    {
        protected List<Device> devices;
        protected int ledCount;
        protected bool firstRun = false;

        public virtual void Init(List<Device> devices)
        {
            this.devices = devices;
            ledCount = 0;

            foreach (var device in devices)
            {
                ledCount += device.LedCount;
            }

            firstRun = true;
        }

        public abstract bool Step(HardwareMeasurements.Measurements measurements);

        protected void SetPixels(int r, int g, int b)
        {
            foreach (var device in devices)
            {
                device.SetPixels(r, g, b);
            }
        }

        protected void SetPixel(int i, int r, int g, int b)
        {
            foreach (var device in devices)
            {
                if (i < device.LedCount)
                {
                    device.SetPixel(i, r, g, b);
                    return;
                }
                i -= device.LedCount;
            }
        }
    }
}
