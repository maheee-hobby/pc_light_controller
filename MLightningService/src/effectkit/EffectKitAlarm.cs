﻿using MLightningService.service;

namespace MLightningService.effectkit
{
    public class EffectKitAlarm : EffectKitSimple
    {
        private int t = 0;
        private int p = 0;

        public override bool Step(HardwareMeasurements.Measurements measurements)
        {
            p = 1 - p;
            if (p == 0)
            {
                return false;
            }

            int b = 0;
            for (int i = 0; i < ledCount; ++i)
            {
                if ((i + t) % 2 == 0)
                {
                    SetPixel(i, 0, 0, 0);
                }
                else
                {
                    if (b % 2 == 0)
                    {
                        SetPixel(i, 255, 0, 0);
                    }
                    else
                    {
                        SetPixel(i, 0, 0, 255);
                    }
                    b = 1 - b;
                }
            }

            t = 1 - t;

            return false;
        }
    }
}
