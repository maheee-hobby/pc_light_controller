﻿using MLightningService.service;
using System.Collections.Generic;

namespace MLightningService.effectkit
{
    public interface IEffectKit
    {
        void Init(List<Device> devices);
        bool Step(HardwareMeasurements.Measurements measurements);
    }
}