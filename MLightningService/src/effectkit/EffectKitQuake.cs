﻿using MLightningService.service;

/*
"mmnmmommommnonmmonqnmmo";                              // 1 FLICKER (first variety)
"abcdefghijklmnopqrstuvwxyzyxwvutsrqponmlkjihgfedcba";  // 2 SLOW STRONG PULSE
"mmmmmaaaaammmmmaaaaaabcdefgabcdefg";                   // 3 CANDLE (first variety)
"mamamamamama";                                         // 4 FAST STROBE
"jklmnopqrstuvwxyzyxwvutsrqponmlkj";                    // 5 GENTLE PULSE 1
"nmonqnmomnmomomno";                                    // 6 FLICKER (second variety)
"mmmaaaabcdefgmmmmaaaammmaamm";                         // 7 CANDLE (second variety)
"mmmaaammmaaammmabcdefaaaammmmabcdefmmmaaaa";           // 8 CANDLE (third variety)
"aaaaaaaazzzzzzzz";                                     // 9 SLOW STROBE (fourth variety)
"mmamammmmammamamaaamammma";                            // 10 FLUORESCENT FLICKER
"abcdefghijklmnopqrrqponmlkjihgfedcba";                 // 11 SLOW PULSE NOT FADE TO BLACK
*/

namespace MLightningService.effectkit
{
    public class EffectKitQuake : EffectKitSimple
    {
        private readonly string effect;
        private int i = 0;

        public EffectKitQuake(int effectNo)
        {
            switch (effectNo)
            {
                case 0: effect = "mmmaaammmaaammmabcdefaaaammmmabcdefmmmaaaa"; break;
                case 1: effect = "nmonqnmomnmomomno"; break;
                case 2: effect = "mmamammmmammamamaaamammma"; break;
            }
        }

        public override bool Step(HardwareMeasurements.Measurements measurements)
        {
            int c = (effect[i] - 'a') * 9;

            SetPixels(c, c, c);

            ++i;
            if (i >= effect.Length)
            {
                i = 0;
            }

            return false;
        }
    }
}
