﻿using MLightningService.service;

namespace MLightningService.effectkit
{
    public class EffectKitSingleColor : EffectKitSimple
    {
        private readonly int r;
        private readonly int g;
        private readonly int b;

        public EffectKitSingleColor(int r, int g, int b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public override bool Step(HardwareMeasurements.Measurements measurements)
        {
            for (int i = 0; i < ledCount; ++i)
            {
                SetPixel(i, r, g, b);
            }
            return false;
        }
    }
}
