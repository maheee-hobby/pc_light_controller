﻿

using AuraServiceLib;
using HidSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace MLightningService.service
{
    public class AuraControl : Control
    {
        public class AuraDevice : Device
        {
            private readonly IAuraSyncDevice device;
            private readonly IAuraRgbLight[] lightArray; // prevent gc from collecting COM objects

            private bool changed = true;

            internal AuraDevice(string name, int ledCount, IAuraSyncDevice device) : base(name, ledCount)
            {
                this.device = device;

                // saving all the lights to prevent gc from collecting COM objects
                this.lightArray = new IAuraRgbLight[device.Lights.Count];
                int i = 0;
                foreach (IAuraRgbLight light in device.Lights)
                {
                    lightArray[i++] = light;
                }
            }

            private void SetPixel(int pos, uint color)
            {
                if (pos < 0 || pos >= LedCount)
                {
                    throw new Exception("Index out of range!");
                }
                lightArray[pos].Color = color;
                changed = true;
            }

            public override void SetPixel(int pos, int r, int g, int b)
            {
                SetPixel(pos, (uint)b << 16 | (uint)g << 8 | (uint)r);
            }

            public override void SetPixels(int r, int g, int b)
            {
                for (int i = 0; i < lightArray.Length; ++i)
                {
                    lightArray[i].Color = (uint)b << 16 | (uint)g << 8 | (uint)r;
                }
                changed = true;
            }

            public override void Apply(bool force = false)
            {
                if (changed || force)
                {
                    device.Apply();
                }
            }
        }

        // sdk
        private IAuraSdk2 auraSdk;
        private IAuraSyncDeviceCollection auraDevices;

        public override void Start()
        {
            auraSdk = new AuraSdk() as IAuraSdk2;
            auraSdk.SwitchMode();
            auraDevices = auraSdk.Enumerate(0); // get all devices
        }

        public override void Stop()
        {
            auraSdk.ReleaseControl(0);
        }

        public override int GetDeviceCount()
        {
            return auraDevices.Count;
        }

        public override string GetDeviceInfo(int i)
        {
            return auraDevices[i].Name;
        }

        public override Device GetDevice(int i, int maxLedCount)
        {
            var device = auraDevices[i];
            var result = new AuraDevice(device.Name, Math.Min(device.Lights.Count, maxLedCount), device);
            return result;
        }
    }
}