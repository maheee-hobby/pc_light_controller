﻿using LibreHardwareMonitor.Hardware;
using System.Linq;
using System.Collections.Generic;
using HidSharp.Utility;
using System.Threading.Tasks;

namespace MLightningService.service
{
    public class HardwareMeasurements
    {
        public class Measurements
        {
            public float? cpuTemp = null;
            public float? gpuTemp = null;
            public float? cpuLoad = null;
            public float? gpuLoad = null;

            public float? memoryLoad = null;
        }

        private readonly Computer computer = new Computer
        {
            IsGpuEnabled = true,
            IsCpuEnabled = true,
            IsMemoryEnabled = true,
        };

        private IHardware cpuHardware;
        private IHardware gpuHardware;
        private IHardware memoryHardware;

        private bool IsCpu(IHardware hardware)
        {
            return hardware.HardwareType == HardwareType.Cpu;
        }
        private bool IsGpu(IHardware hardware)
        {
            return hardware.HardwareType == HardwareType.GpuAmd || hardware.HardwareType == HardwareType.GpuNvidia || hardware.HardwareType == HardwareType.GpuIntel;
        }
        private bool IsMemory(IHardware hardware)
        {
            return hardware.HardwareType == HardwareType.Memory;
        }
        private bool IsTemp(ISensor sensor, string name)
        {
            return sensor.SensorType == SensorType.Temperature && sensor.Name.Contains(name);
        }
        private bool IsLoad(ISensor sensor, string name)
        {
            return sensor.SensorType == SensorType.Load && sensor.Name.Contains(name);
        }

        public void Start()
        {
            computer.Open();
        }

        public void Stop()
        {
            computer.Close();
        }

        public void Init()
        {
            cpuHardware = computer.Hardware.Where(hardware => IsCpu(hardware)).Single();
            gpuHardware = computer.Hardware.Where(hardware => IsGpu(hardware)).Single();
            memoryHardware = computer.Hardware.Where(hardware => IsMemory(hardware)).Single();

            UpdateMeasurements();
        }

        public void UpdateMeasurements()
        {
            cpuHardware.Update();
            gpuHardware.Update();
            memoryHardware.Update();
        }

        public Task UpdateMeasurementsAsync()
        {
            return Task.Run(() => UpdateMeasurements());
        }

        public Measurements GetMeasurements()
        {
            var cpuSensors = cpuHardware.Sensors;
            var cpuTempSensor = cpuSensors.Where(sensor => IsTemp(sensor, "CPU Package")).Single();
            var cpuLoadSensor = cpuSensors.Where(sensor => IsLoad(sensor, "CPU Total")).Single();

            var gpuSensors = gpuHardware.Sensors;
            var gpuTempSensor = gpuSensors.Where(sensor => IsTemp(sensor, "GPU Core")).Single();
            var gpuLoadSensor = gpuSensors.Where(sensor => IsLoad(sensor, "GPU Core")).Single();

            var memorySensors = memoryHardware.Sensors;
            var memoryLoadSensor = memorySensors.Where(sensor => IsLoad(sensor, "Memory")).First();

            Measurements result = new Measurements
            {
                cpuTemp = cpuTempSensor.Value.GetValueOrDefault(),
                cpuLoad = cpuLoadSensor.Value.GetValueOrDefault(),

                gpuTemp = gpuTempSensor.Value.GetValueOrDefault(),
                gpuLoad = gpuLoadSensor.Value.GetValueOrDefault(),

                memoryLoad = memoryLoadSensor.Value.GetValueOrDefault(),
            };

            return result;
        }

        public string GetReport()
        {
            return computer.GetReport();
        }
    }
}
