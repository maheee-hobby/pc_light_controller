﻿
namespace MLightningService.service
{
    public abstract class Control
    {
        public abstract void Start();

        public abstract void Stop();

        public abstract int GetDeviceCount();

        public abstract string GetDeviceInfo(int i);

        public abstract Device GetDevice(int i, int maxLedCount);
    }
}
