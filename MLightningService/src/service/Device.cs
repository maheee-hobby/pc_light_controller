﻿using System.Threading.Tasks;

namespace MLightningService.service
{
    public abstract class Device
    {
        public readonly string Name;
        public readonly int LedCount;

        public Device(string name, int ledCount)
        {
            Name = name;
            LedCount = ledCount;
        }

        public abstract void SetPixel(int pos, int r, int g, int b);

        public virtual void SetPixels(int r, int g, int b)
        {
            for (int i = 0; i < LedCount; ++i)
            {
                SetPixel(i, r, g, b);
            }
        }

        public virtual void TurnOff()
        {
            SetPixels(0, 0, 0);
        }

        public abstract void Apply(bool force = false);

        public virtual Task ApplyAsync()
        {
            return Task.Run(() => Apply());
        }
    }
}
