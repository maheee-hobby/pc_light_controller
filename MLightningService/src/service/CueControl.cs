﻿using MLightningService.corsair;
using System;
using System.Collections.Generic;

namespace MLightningService.service
{
    public class CueControl : Control
    {
        public class CueDevice : Device
        {
            private readonly int deviceId;
            private readonly LedPositions ledPositions;

            private readonly bool syncer;

            internal CueDevice(string name, int ledCount, int deviceId, LedPositions ledPositions, bool syncer) : base(name, ledCount)
            {
                this.deviceId = deviceId;
                this.ledPositions = ledPositions;
                this.syncer = syncer;
            }

            public override void SetPixel(int pos, int r, int g, int b)
            {
                if (pos < 0 || pos >= LedCount)
                {
                    throw new Exception("Index out of range!");
                }
                List<LedColor> colors = new List<LedColor>
                    {
                        new LedColor(ledPositions.Positions[pos].LedId, r, g, b)
                    };
                CueSdk.SetLedsColorsBufferByDeviceIndex(deviceId, colors);
            }

            public override void Apply(bool force = false)
            {
                if (syncer)
                {
                    CueSdk.SetLedsColorsFlushBuffer();
                }
            }
        }

        private bool isFirst = true;

        public override void Start()
        {
            CueSdk.LoadSdk();
            CueSdk.PerformProtocolHandshake();
            CueSdk.RequestControl(ControlType.ExclusiveLightingControl);
        }

        public override void Stop()
        {
            CueSdk.ReleaseControl(ControlType.ExclusiveLightingControl);
            // CueSdk.UnloadSdk();
        }

        public override int GetDeviceCount()
        {
            return CueSdk.GetDeviceCount();
        }

        public override string GetDeviceInfo(int i)
        {
            return CueSdk.GetDeviceInfo(i).Model;
        }

        public override Device GetDevice(int i, int maxLedCount)
        {
            var deviceInfo = CueSdk.GetDeviceInfo(i);
            var ledPositions = CueSdk.GetLedPositionsByDeviceIndex(i);
            if (isFirst)
            {
                isFirst = false;
                return new CueDevice(deviceInfo.Model, Math.Min(deviceInfo.LedsCount, maxLedCount), i, ledPositions, true);
            }
            return new CueDevice(deviceInfo.Model, Math.Min(deviceInfo.LedsCount, maxLedCount), i, ledPositions, false);
        }
    }
}
