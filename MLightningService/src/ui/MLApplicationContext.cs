﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FontAwesome.Sharp;
using Microsoft.Win32;
using MLightningService.effectkit;

namespace MLightningService.ui
{
    public class MLApplicationContext : ApplicationContext
    {
        private readonly MLApplication application;

        private readonly NotifyIcon trayIcon;
        private readonly System.Drawing.Icon iconStarted;
        private readonly System.Drawing.Icon iconWorking;
        private readonly System.Drawing.Icon iconStopped;

        private readonly ToolStripMenuItem iconStart;
        private readonly ToolStripMenuItem iconRestart;
        private readonly ToolStripMenuItem iconStop;

        private readonly ToolStripMenuItem iconEffectDefault;
        private readonly ToolStripMenuItem iconEffectDefault2;
        private readonly ToolStripMenuItem iconEffectScan;
        private readonly ToolStripMenuItem iconEffectQuake1;
        private readonly ToolStripMenuItem iconEffectQuake2;
        private readonly ToolStripMenuItem iconEffectQuake3;
        private readonly ToolStripMenuItem iconEffectAlarm;
        private readonly ToolStripMenuItem iconEffectDemo;

        private readonly ToolStripMenuItem iconEffectWhite;
        private readonly ToolStripMenuItem iconEffectRed;
        private readonly ToolStripMenuItem iconEffectGreen;
        private readonly ToolStripMenuItem iconEffectBlue;
        private readonly ToolStripMenuItem iconEffectBlack;

        private readonly ToolStripMenuItem iconExit;

        private readonly ToolStripMenuItem[] effectIcons;
        private readonly ToolStripMenuItem[] effectIcons2;

        enum ApplicationStatus
        {
            STARTED,
            STOPPED,
            WORKING
        }

        private ApplicationStatus status;
        private string selectedEffect;

        public MLApplicationContext()
        {
            application = new MLApplication();
            application.Init();

            Bitmap CreateIcon(IconChar iconChar, Color color) =>
                    iconChar.ToBitmap(color);

            ToolStripMenuItem CreateMenuItem(string text, IconChar icon, Color color, EventHandler handler, string name) =>
                    new ToolStripMenuItem(text, CreateIcon(icon, color), handler, name);

            ToolStripMenuItem CreateSimpleMenuItem(string text, IconChar icon, Color color, EventHandler handler) =>
                    new ToolStripMenuItem(text, CreateIcon(icon, color), handler);

            iconStarted = System.Drawing.Icon.FromHandle(CreateIcon(IconChar.Lightbulb, Color.White).GetHicon());
            iconWorking = System.Drawing.Icon.FromHandle(CreateIcon(IconChar.Lightbulb, Color.DarkOrange).GetHicon());
            iconStopped = System.Drawing.Icon.FromHandle(CreateIcon(IconChar.Lightbulb, Color.DarkGray).GetHicon());

            iconStart = CreateSimpleMenuItem("Start", IconChar.Play, Color.DarkGreen, Start);
            iconRestart = CreateSimpleMenuItem("Restart", IconChar.Repeat, Color.DarkOrange, Restart);
            iconStop = CreateSimpleMenuItem("Stop", IconChar.Stop, Color.DarkRed, Stop);

            iconEffectDefault = CreateMenuItem("Effect Default", IconChar.Hurricane, Color.DarkBlue, ChangeEffect, "effectDefault");
            iconEffectDefault2 = CreateMenuItem("Effect Default 2", IconChar.Hurricane, Color.DarkBlue, ChangeEffect, "effectDefault2");
            iconEffectScan = CreateMenuItem("Effect Scan", IconChar.Radiation, Color.DarkBlue, ChangeEffect, "effectScan");
            iconEffectQuake1 = CreateMenuItem("Effect Quake 1", IconChar.Om, Color.DarkBlue, ChangeEffect, "effectQuake1");
            iconEffectQuake2 = CreateMenuItem("Effect Quake 2", IconChar.Om, Color.DarkBlue, ChangeEffect, "effectQuake2");
            iconEffectQuake3 = CreateMenuItem("Effect Quake 3", IconChar.Om, Color.DarkBlue, ChangeEffect, "effectQuake3");
            iconEffectAlarm = CreateMenuItem("Effect Alarm", IconChar.VolumeOff, Color.DarkBlue, ChangeEffect, "effectAlarm");
            iconEffectDemo = CreateMenuItem("Effect Demo", IconChar.Fire, Color.DarkBlue, ChangeEffect, "effectDemo");

            iconEffectWhite = CreateMenuItem("Color White", IconChar.DotCircle, Color.White, ChangeEffect, "effectWhite");
            iconEffectRed = CreateMenuItem("Color Red", IconChar.DotCircle, Color.Red, ChangeEffect, "effectRed");
            iconEffectGreen = CreateMenuItem("Color Green", IconChar.DotCircle, Color.Green, ChangeEffect, "effectGreen");
            iconEffectBlue = CreateMenuItem("Color Blue", IconChar.DotCircle, Color.Blue, ChangeEffect, "effectBlue");
            iconEffectBlack = CreateMenuItem("Color Black", IconChar.DotCircle, Color.Black, ChangeEffect, "effectBlack");

            iconExit = CreateSimpleMenuItem("Exit", IconChar.Xmark, Color.Black, Exit);

            effectIcons = new ToolStripMenuItem[]
            {
                iconEffectDefault,
                iconEffectDefault2,
                iconEffectScan,
                iconEffectQuake1,
                iconEffectQuake2,
                iconEffectQuake3,
                iconEffectAlarm,
                iconEffectDemo,
            };

            effectIcons2 = new ToolStripMenuItem[]
            {
                iconEffectWhite,
                iconEffectRed,
                iconEffectGreen,
                iconEffectBlue,
                iconEffectBlack
            };

            var contextMenuStrip = new ContextMenuStrip();
            contextMenuStrip.Items.Add(iconStart);
            contextMenuStrip.Items.Add(iconRestart);
            contextMenuStrip.Items.Add(iconStop);
            contextMenuStrip.Items.Add(new ToolStripSeparator());
            contextMenuStrip.Items.AddRange(effectIcons);
            contextMenuStrip.Items.Add(new ToolStripSeparator());
            contextMenuStrip.Items.AddRange(effectIcons2);
            contextMenuStrip.Items.Add(new ToolStripSeparator());
            contextMenuStrip.Items.Add(iconExit);

            // Initialize Tray Icon
            trayIcon = new NotifyIcon()
            {
                Icon = iconStopped,
                ContextMenuStrip = contextMenuStrip,
                Visible = true
            };

            UpdateState(ApplicationStatus.STOPPED, null);

            // Exit application properly when shutting down
            SystemEvents.SessionEnding += ExitFast;
        }

        private void SetEffectIconsEnabled(bool enabledState) {
            foreach (var icon in effectIcons)
            {
                icon.Enabled = enabledState;
            }
            foreach (var icon in effectIcons2)
            {
                icon.Enabled = enabledState;
            }
        }

        private void UpdateState(ApplicationStatus? newStatus, string newEffect)
        {
            if (newStatus != null)
            {
                status = (ApplicationStatus)newStatus;
            }
            if (newEffect != null)
            {
                selectedEffect = newEffect;
                application.SelectEffectKit(selectedEffect);
            }

            switch (status)
            {
                case ApplicationStatus.STARTED:
                    trayIcon.Icon = iconStarted;
                    trayIcon.Text = "Started!";

                    iconStart.Enabled = false;
                    iconRestart.Enabled = true;
                    iconStop.Enabled = true;

                    SetEffectIconsEnabled(true);
                    break;
                case ApplicationStatus.STOPPED:
                    trayIcon.Icon = iconStopped;
                    trayIcon.Text = "Stopped!";

                    iconStart.Enabled = true;
                    iconRestart.Enabled = false;
                    iconStop.Enabled = false;

                    SetEffectIconsEnabled(false);
                    break;
                case ApplicationStatus.WORKING:
                    trayIcon.Icon = iconWorking;
                    trayIcon.Text = "Working ...";

                    iconStart.Enabled = false;
                    iconRestart.Enabled = false;
                    iconStop.Enabled = false;

                    SetEffectIconsEnabled(false);
                    break;
            }
        }

        void Start(object sender, EventArgs e)
        {
            UpdateState(ApplicationStatus.WORKING, null);

            application.Start();

            UpdateState(ApplicationStatus.STARTED, "effectDefault");
        }

        void Restart(object sender, EventArgs e)
        {
            UpdateState(ApplicationStatus.WORKING, null);

            application.Stop();
            application.Start();

            UpdateState(ApplicationStatus.STARTED, "effectDefault");
        }

        void Stop(object sender, EventArgs e)
        {
            UpdateState(ApplicationStatus.WORKING, null);

            application.Stop();

            UpdateState(ApplicationStatus.STOPPED, null);
        }

        void ChangeEffect(object sender, EventArgs e)
        {
            if (!(sender is ToolStripMenuItem senderItem))
            {
                return;
            }
            UpdateState(null, senderItem.Name);
        }

        private void DoExit(bool fast)
        {
            if (status != ApplicationStatus.STOPPED)
            {
                UpdateState(ApplicationStatus.WORKING, null);
                application.Stop(fast);
            }
            trayIcon.Visible = false;
            Application.Exit();
        }

        void Exit(object sender, EventArgs e)
        {
            DoExit(false);
        }

        void ExitFast(object sender, EventArgs e)
        {
            DoExit(true);
        }
    }
}
