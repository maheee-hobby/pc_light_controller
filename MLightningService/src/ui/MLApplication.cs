﻿using MLightningService.effectkit;
using MLightningService.job;
using MLightningService.service;
using System.Collections.Generic;
using System.Threading;

namespace MLightningService.ui
{
    public class MLApplication
    {
        private AuraControl auraControl = new AuraControl();
        private CueControl cueControl = new CueControl();
        private HardwareMeasurements hardwareMeasurements = new HardwareMeasurements();

        private Worker worker = new Worker();
        private Thread thread;

        private List<(string, int)> deviceSetup;
        private List<Device> devices;

        private readonly Dictionary<string, IEffectKit> effectKits = new Dictionary<string, IEffectKit>();

        public void Init()
        {
            // create subservices
            auraControl = new AuraControl();
            cueControl = new CueControl();
            hardwareMeasurements = new HardwareMeasurements();

            deviceSetup = new List<(string, int)>() {
                ("ENE_RGB_For_ASUS0", 8), // RAM 1
                ("ENE_RGB_For_ASUS1", 8), // RAM 2
                ("Mainboard_Master",  3), // Mainboard
                ("AddressableStrip 1", 24), // LED Strip
                ("AddressableStrip 2", 16), // Graphics Card Holder
                ("AddressableStrip 3",  8), // Front Fan
                ("H150i ELITE",        9999), // Cooler (knows amount automatically)
                ("Lighting Node CORE", 9999), // Fans (Top & Bottom) (knows amount automatically)
            };

            effectKits.Add("effectDefault", new EffectKitDefault());
            effectKits.Add("effectDefault2", new EffectKitDefault2());
            effectKits.Add("effectScan", new EffectKitScan());
            effectKits.Add("effectQuake1", new EffectKitQuake(0));
            effectKits.Add("effectQuake2", new EffectKitQuake(1));
            effectKits.Add("effectQuake3", new EffectKitQuake(2));
            effectKits.Add("effectAlarm", new EffectKitAlarm());
            effectKits.Add("effectDemo", new EffectKitDemo());

            effectKits.Add("effectWhite", new EffectKitSingleColor(255, 255, 255));
            effectKits.Add("effectRed", new EffectKitSingleColor(255, 0, 0));
            effectKits.Add("effectGreen", new EffectKitSingleColor(0, 255, 0));
            effectKits.Add("effectBlue", new EffectKitSingleColor(0, 0, 255));
            effectKits.Add("effectBlack", new EffectKitSingleColor(0, 0, 0));
        }

        public void Start()
        {
            // start subservices
            auraControl.Start();
            cueControl.Start();
            hardwareMeasurements.Start();
            hardwareMeasurements.Init();

            worker = new Worker();
            devices = worker.Init(auraControl, cueControl, hardwareMeasurements, deviceSetup);
            thread = new Thread(new ThreadStart(worker.Job));
            thread.Start();
        }

        public void Stop(bool fast = false)
        {
            worker.Stop();
            thread?.Join();

            // reset subservices
            if (!fast)
            {
                auraControl.Stop();
                cueControl.Stop();
                hardwareMeasurements.Stop();
            }
        }

        public void SelectEffectKit(string name)
        {
            var effectKit = effectKits[name];
            effectKit.Init(devices);
            worker.SetEffectKit(effectKit);
        }
    }
}
