﻿using MLightningService.effect;
using MLightningService.effectkit;
using MLightningService.service;
using MLightningService.src.job;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static MLightningService.util.Util;

namespace MLightningService.job
{
    public class Worker
    {
        private volatile bool cancel = false;

        private HardwareMeasurements hardwareMeasurements;

        private List<Device> devices;

        private volatile IEffectKit effectKit;

        public void SetEffectKit(IEffectKit effectKit)
        {
            this.effectKit = effectKit;
        }

        public List<Device> Init(AuraControl auraControl, CueControl cueControl, HardwareMeasurements hardwareMeasurements, List<(string, int)> deviceSetup)
        {
            this.hardwareMeasurements = hardwareMeasurements;
            devices = DeviceMapper.Map(auraControl, cueControl, deviceSetup);
            return devices;
        }

        public void Stop()
        {
            cancel = true;
        }

        public void Job()
        {
            cancel = false;

            long now = Now();
            long lastTs = now;
            int waitTime;
            Task hardwareUpdateTask = Task.CompletedTask;

            var measurements = hardwareMeasurements.GetMeasurements();

            // do led stuff
            while (!cancel)
            {
                effectKit?.Step(measurements);

                // Update all
                // Task.WaitAll(devices.Select(el => el.ApplyAsync()).ToArray());
                foreach (var device in devices)
                {
                    device.Apply();
                }

                if (hardwareUpdateTask.IsCompleted)
                {
                    measurements = hardwareMeasurements.GetMeasurements();
                    hardwareUpdateTask = hardwareMeasurements.UpdateMeasurementsAsync();
                }

                // Wait for next execution
                now = Now();
                waitTime = Math.Max(5, 60 - (int)(now - lastTs));
                lastTs = now;
                Thread.Sleep(waitTime);
            }
        }
    }
}
