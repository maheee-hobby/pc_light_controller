﻿using MLightningService.service;
using System;
using System.Collections.Generic;

namespace MLightningService.src.job
{
    public static class DeviceMapper
    {
        private static void Map(Control control, List<Device> result, List<(string, int)> deviceList)
        {
            for (int i = 0; i < control.GetDeviceCount(); ++i)
            {
                string deviceName = control.GetDeviceInfo(i);

                for (int j = 0; j < deviceList.Count; ++j)
                {
                    if (deviceName.Equals(deviceList[j].Item1))
                    {
                        result[j] = control.GetDevice(i, deviceList[j].Item2);
                    }
                }
            }
        }

        public static List<Device> Map(AuraControl auraControl, CueControl cueControl, List<(string, int)> deviceList)
        {
            List<Device> result = new List<Device>(new Device[deviceList.Count]);

            // assign aura devices
            Map(auraControl, result, deviceList);

            // assign cue devices
            Map(cueControl, result, deviceList);

            return result;
        }
    }
}
