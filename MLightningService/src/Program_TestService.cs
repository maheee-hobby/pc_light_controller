﻿using System;
using System.Collections.Generic;
using System.Threading;
using MLightningService.job;
using MLightningService.service;
using static MLightningService.util.Util;

namespace MLightningService
{
    internal static class ProgramTestService
    {
        static void Main()
        {
            // check if we are admin
            if (!IsCurrentUserInAdminGroup())
            {
                throw new Exception("Need admin rights!");
            }

            var deviceSetup = new List<(string, int)>() {
                ("ENE_RGB_For_ASUS0", 8), // RAM 1
                ("ENE_RGB_For_ASUS1", 8), // RAM 2
                ("Mainboard_Master",  3), // Mainboard
                ("AddressableStrip 1", 24), // LED Strip
                ("AddressableStrip 2", 16), // Graphics Card Holder
                ("AddressableStrip 3",  8), // Front Fan
                ("H150i ELITE",        9999), // Cooler
                ("Lighting Node CORE", 9999), // Fans (Top & Bottom)
            };

            // create subservices
            AuraControl auraControl = new AuraControl();
            CueControl cueControl = new CueControl();
            HardwareMeasurements hardwareMeasurements = new HardwareMeasurements();
            Worker worker = new Worker();

            // initialize subservices
            auraControl.Start();
            cueControl.Start();
            hardwareMeasurements.Start();
            hardwareMeasurements.Init();

            worker.Init(auraControl, cueControl, hardwareMeasurements, deviceSetup);
            Thread thread = new Thread(new ThreadStart(worker.Job));
            thread.Start();

            Console.ReadLine();

            worker.Stop();
            thread.Join();

            // reset subservices
            auraControl.Stop();
            cueControl.Stop();
            hardwareMeasurements.Stop();
        }
    }
}
